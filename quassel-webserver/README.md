## quassel-webserver ##
Dockerfile for [quassel-webserver](https://github.com/magne4000/quassel-webserver) by magne4000.

A web client for Quassel (requires a running quasselcore)

### Usage ###
Out of the box, it provides a client that can connect to any quassel core.

```
docker run -d --name quassel-webserver -p 443:443 jakexks/quassel-webserver
```

Alternatively, you can specify advanced options by setting the following environment variables:

  * ```QUASSEL_HOST``` - the address of your quassel core (default empty)
  * ```QUASSEL_PORT``` - the port it is listening on (default 4242)
  * ```FORCE_DEFAULT``` - Only allow connections to the above core (default false)
  * ```WEBSERVER_MODE``` - http or https (default https)
  * ```WEBSERVER_PORT``` - the port to listen on (default 443)

Full example:

```
docker run -d --name quassel-webserver -p 8080:80 -e QUASSEL_HOST=mybnc.tld \
    -e QUASSEL_PORT=9001 -e FORCE_DEFAULT=true -e WEBSERVER_MODE=http \
    -e WEBSERVER_PORT=80 jakexks/quassel-webserver
```
